
class Command():
    def __init__(self):
        #raise NotImplementedError('subclasses must override foo()!')
        pass

    def parse_and_execute(self, *args):
        raise NotImplementedError('subclasses must override foo()!')

    def help(self):
        raise NotImplementedError('subclasses must override foo()!')
