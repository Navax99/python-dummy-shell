
import sys
import command
import logger

class Hello(command.Command):
    def __init__(self):
        pass

    def run(self, num):
        for runs in range(num):
            print("Hello")

    def help(self):
        print("Usage: hello [N]")
        print("Say 'Hello' N time")
        print("N\tnumber of 'Hello' to print (default is 1)")

    def parse_and_execute(self, *args):
        # args
        n = 1

        if len(args) > 1:
            logger.error('invalid number of arguments. Expected 1')
            self.help()
            return
        elif len(args) == 1:
            try:
                n = int(args[0])
                if n < 0:
                    raise Exception() # Jump to the trap
            except:
                logger.error('N must be a natural number')
                return

        self.run(n)
