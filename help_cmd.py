
import sys
import command
import logger
import commands
import inspect

class Help(command.Command):
    def __init__(self, commands_list):
        self.commands_list = {}
        self.commands_list = commands_list
        # for name, cmd in inspect.getmembers(commands):
        #     if inspect.isclass(cmd):
        #         name = name.lower()
        #         self.commands_list[name] = cmd()

    def run(self, name):
       self.commands_list[name].help()

    def help(self):
        print("Usage: hello NAME")
        print("Print help from a command")

    def parse_and_execute(self, *args):
        # args
        name = ''

        if len(args) > 1 or len(args) == 0:
            logger.error('invalid number of arguments. Expected 1')
            self.help()
            return
        elif len(args) == 1:
            name = args[0]
            if not (name in self.commands_list):
                logger.error(str(name) + " no such command. Couldn't find documentation"  )
                return

        self.run(name)
