#!/usr/bin/python2

''' This file contains code from https://gist.github.com/rduplain/899f6a5e583a85668822'''

from __future__ import print_function
import code
import shlex
import sys
import inspect

from sys import stderr

import commands # This file contains the list of all commands
import help_cmd

class CommandRunner():
    ''' Command parser '''
    def __init__(self):
        self.commands_list = {}

        # Common commands
        for name, cmd in inspect.getmembers(commands):
            if inspect.isclass(cmd):
                name = name.lower()
                self.commands_list[name] = cmd()

        # Special commands
        self.commands_list[help_cmd.Help.__name__.lower()] = help_cmd.Help(self.commands_list)

    def run(self, line):
        tokens = shlex.split(line, comments=True)
        if len(tokens) == 0:
            return
        command, args = tokens[0], tokens[1:]
        if command not in self.commands_list:
            print('{}: no such command'.format(command), file=stderr)
            return
        result = self.commands_list[command].parse_and_execute(*args)
        if result is not None:
            print(result)


class Console():
    ps1 = '> '
    ps2 = '. '

    def __init__(self, runner):
        self.runner = runner

    def run(self, fd):
        for line in fd:
            self.runner.run(line)

    def interact(self, locals=None):
        class LambdaConsole(code.InteractiveConsole):
            def runsource(code_console, source, filename=None, symbol=None):
                # Return True if more input needed, else False.
                try:
                    self.runner.run(source)
                except SystemExit:
                    raise
                except:
                    code_console.showtraceback()
                return False

        # import readline to support line editing within console session.
        try:
            import readline; readline
        except ImportError:
            pass

        # Patch ps1 & ps2 for interaction. Note sys.psX may be unset.
        ps1, ps2 = getattr(sys, 'ps1', None), getattr(sys, 'ps2', None)
        try:
            sys.ps1, sys.ps2 = self.ps1, self.ps2
            LambdaConsole(locals=locals, filename="<demo>").interact(banner='')
        finally:
            sys.ps1, sys.ps2 = ps1, ps2

    def run_in_main(self, fd=None, interact=False):
        if fd is None:
            fd = sys.stdin
        if fd.isatty():
            self.interact()
        else:
            try:
                self.run(fd=fd)
            except Exception as err:
                print(err, file=stderr)
                return 1
        return 0




def main(fd=None):
    runner = CommandRunner()
    return Console(runner).run_in_main(fd)


if __name__ == '__main__':
    sys.exit(main())
